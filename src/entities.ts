
export interface Article {
    id?:number;
    name:string;
    content: string;
    date: string;
    author: string;
    img: string;
}
