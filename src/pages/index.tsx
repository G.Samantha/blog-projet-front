import { fetchAllArticle } from "@/article-service";
import ItemArticle from "@/compenents/ItemArticle";
import Navbar from "@/compenents/Navbar";
import { Article } from "@/entities";
import { useEffect, useState } from "react";

export default function Index() {
  const [article, setArticle] = useState<Article[]>([]);

  useEffect(() => {
  //  async () => {
  //     const data = await fetchAllArticle();
  //     console.log(data);
      
  //   }
  //  console.log(fetchData());

  //  setArticle(fetchData())
   
    fetchAllArticle().then((data) => {
      setArticle(data);
    });
  }, [article]);

  return (
    <>
      {Navbar()}
      <div className="bgIndex Indexsection">
        <div className="divTitle">
          <h1 className="h1 title">Un developpeur, un compagnon</h1>
        </div>
      </div>
      <div className="divParagraphe">
        <p>
         Bienvenue dans nos blog conssacrés à des developpeurs et des compagnons d'exceptions ! 
         < br/>Chaque mois nous façon le portrait d'un invité, venez les découvrirs dans nos articles.
        </p>
      </div>
      <div className="container-fluid">
        <div className="row d-flex justify-content-center">
          {article.map((item) => (
            <ItemArticle key={item.id} article={item} />
          ))}
         
        </div>
        
      </div>
    </>
  );
}
