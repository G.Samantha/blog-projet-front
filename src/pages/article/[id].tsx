import {
  deleteArticle,
  fetchOneArticle,
  updateArticle,
} from "@/article-service";
import FormArticle from "@/compenents/FormArticle";
import ItemArticle from "@/compenents/ItemArticle";
import Navbar from "@/compenents/Navbar";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function ArtciclePage() {
  const router = useRouter();
  const { id } = router.query;
  const [article, setArticle] = useState<Article>();
  const [showEdit, setShowEdit] = useState(false);

  useEffect(() => {
    if (!id) {
      return;
    }
    fetchOneArticle(Number(id))
      .then((data) => setArticle(data))
      .catch((error) => {
        // console.log(error);
        if (error.response.data.status == 404) {
          return router.push("/404");
        }
      });
  }, [id, article?.id]);

  async function remove() {
    await deleteArticle(id);
    router.push("/");
  }

  async function update(article: Article) {
    const updated = await updateArticle(article);
    setArticle(updated);
  }

  function toggle() {
    setShowEdit(!showEdit);
  }

  if (!article) {
    return <p>Loading...</p>;
  }

  return (
    <>
      {Navbar()}

      <div className="bgarticle">
        <div className=" backgroundgrayArticle">
          <h1 className="padArticle">{article.name}</h1>
          <img className="imgArticle" src={article.img} alt="..." />
          <p className="padArticle textArticle">
            {article.author}-{new Date(article.date).toLocaleDateString()}
          </p>
          <p className="padArticle ">{article.content}</p>
        </div>

        <div className="articleButtonsContainer">
          <button
            className="btn greyBackground btn-outline-danger"
            onClick={remove}
          >
            Delete
          </button>
          <button
            className="btn greyBackground btn-outline-dark"
            onClick={toggle}
          >
            Edit
          </button>
        </div>
        <div className="articleform">
          {showEdit && (
            <>
              <FormArticle edited={article} onSubmit={update} />
            </>
          )}
        </div>
      </div>
    </>
  );
}
