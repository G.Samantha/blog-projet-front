import { postArticle } from "@/article-service";
import FormArticle from "@/compenents/FormArticle";
import Navbar from "@/compenents/Navbar";
import { Article } from "@/entities";
import { useRouter } from "next/router";

export default function addArticle() {
    const router = useRouter();

    async function addArticle(article:Article){
        const added = await postArticle(article);
        router.push('/article/'+added.id);
    }

    return (
        <>
        {Navbar()}
        <div className="bgFormImg">
            <h1 className="titreForm">Ajouter un article</h1>
            <FormArticle onSubmit={addArticle}/>
        </div>
        </>
    );
}
