import { Article } from "@/entities";
import Link from "next/link";
import Navbar from "./Navbar";

interface Props {
  article: Article;
}

export default function ItemArticle({ article }: Props) {
  return (
    <>
      <div className="card m-3" style={{ width: 18 + "rem" }}>
        <img src={article.img} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{article.name}</h5>
          <p className="card-text">
            {article.author}-{new Date(article.date).toLocaleDateString()}
          </p>
          <p className="card-text">
            {article.content.split(" ").slice(0, 10).join(" ")}...
          </p>
          <Link href={"/article/" + article.id} className="btn btn-outline-dark">Voir plus</Link>
        </div>
      </div>
    </>
  );
}
