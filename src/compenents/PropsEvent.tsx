
interface Props {
    onAction:() => void
}

export default function PropEvent({onAction}:Props) {

  return (
    <div>PropEvent <button onClick={onAction}>do stuff</button></div>
  )
}