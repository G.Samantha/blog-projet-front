
interface Props {
    onAction:() => void
}

export default function PropsArticle({onAction}:Props) {

  return (
    <div>PropEvent <button onClick={onAction}>do stuff</button></div>
  )
}