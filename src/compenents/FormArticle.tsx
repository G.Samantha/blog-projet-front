import { Article } from "@/entities";
import router, { useRouter } from "next/router";
import { FormEvent, useState } from "react";
// import Navbar from "./Navbar";

interface Props {
  onSubmit: (article: Article) => void;
  edited?: Article;
}

export default function FormArticle({ onSubmit, edited }: Props) {
  const router = useRouter();
  const [errors, setErrors] = useState("");

  const [article, setArticle] = useState<Article>(
    edited
      ? edited
      : {
          name: "",
          content: "",
          date: "",
          author: "",
          img: "",
        }
  );

  function handleChange(event: any) {
    setArticle({
      ...article,
      [event.target.name]: event.target.value,
    });
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();
    try {
      onSubmit(article);
    } catch (error: any) {
      console.log(error);

      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }
    }
  }

  // event.preventDefault();
  // const added = await postArticle(article);
  // router.push('/article/'+added.id);
  return (
    <>
      <div className="container-fluid">
        <div className="row d-flex justify-content-center ">
          <div className="col-8">
            <form className="form-row bgFrom p-2 m-4" onSubmit={handleSubmit}>
              {errors && <p>{errors}</p>}
              
                <div className="col m-2">
                  <label htmlFor="name" className="textForm">Titre :</label>
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    value={article.name}
                    onChange={handleChange}
                    required
                  />
                </div>

                <div className="col m-2">
                  <label htmlFor="date" className="textForm">Date :</label>
                  <input
                    type="date"
                    name="date"
                    className="form-control"
                    required
                    value={article.date}
                    onChange={handleChange}
                  />
                </div>

                <div className="col m-2">
                  <label htmlFor="author" className="textForm">Auteur :</label>
                  <input
                    type="text"
                    name="author"
                    className="form-control"
                    required
                    value={article.author}
                    onChange={handleChange}
                  />
                </div>

                <div className="col m-2">
                  <label htmlFor="img" className="textForm">Image :</label>
                  <input
                    type="text"
                    name="img"
                    className="form-control"
                    required
                    value={article.img}
                    onChange={handleChange}
                  />
                </div>

                <div className="col m-2">
                  <label htmlFor="content" className="textForm">Contenu :</label>
                  <textarea
                    name="content"
                    className="form-control"
                    required
                    value={article.content}
                    onChange={handleChange}
                  />
                </div>

                <button className="btn btn-outline-success m-2 textForm" type="submit">
                  Envoyer
                </button>
              
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
